# About NoPlayerJoinLagDeath
A Plugin that protects you from joining and dieing \
You can configure all in the **plugins/Sleep/[config.yml](https://gitlab.com/Commandcracker/noplayerjoinlagdeath/-/blob/master/src/main/resources/config.yml)** file.
## Requirements
Sleep currently supports **CraftBukkit**, **Spigot** and [Paper](papermc.io/) **(recommended)**. \
Other server implementations may work, but we don't recommend them as they may cause compatibility issues.
## Links
|Source|Artifacts|Stats|
|:---:|:---:|:---:|
|[Gitlab](https://gitlab.com/Commandcracker/noplayerjoinlagdeath)|[Spigot](https://www.spigotmc.org/resources/noplayerjoinlagdeath.79961)|[bStats](https://bstats.org/plugin/bukkit/NoPlayerJoinLagDeath/7803)|
## Metrics collection
NoPlayerJoinLagDeath collects anonymous server statistics through bStats, an open-source Minecraft statistics service. \
\
[![bStats](https://bstats.org/signatures/bukkit/NoPlayerJoinLagDeath.svg)](https://bstats.org/plugin/bukkit/NoPlayerJoinLagDeath/7803) \
If you'd like to disable metrics collection via bStats, you can edit the **plugins/bStats/config.yml** file.