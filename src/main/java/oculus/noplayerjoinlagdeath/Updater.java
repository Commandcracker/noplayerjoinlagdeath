package oculus.noplayerjoinlagdeath;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Updater implements Listener {

    private final String currentversion;
    private String latestversion = null;
    private final NoPlayerJoinLagDeath plugin = NoPlayerJoinLagDeath.getInstance();

    @EventHandler
    public void onjoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (p.hasPermission("sleep.notify")) {
            if (latestversion != null) {
                if (!currentversion.equals(latestversion)) {
                    p.sendMessage("§7A new update for §b" + plugin.getDescription().getName() + "§7 is available! (§e" + currentversion + " §7-> §a" + latestversion + "§7)\nDownload: §b" + plugin.getDescription().getWebsite() + "§7.");
                }
            } else {
                p.sendMessage("§4Could not check for Updates.");
            }
        }
    }

    public Updater() {
        currentversion = plugin.getDescription().getVersion();

        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            @Override
            public void run() {

                URL url = null;
                try {
                    url = new URL("https://gitlab.com/Commandcracker/noplayerjoinlagdeath/-/raw/master/pom.xml");
                } catch (MalformedURLException e) {
                    NoPlayerJoinLagDeath.pluginmessage("§4URL is Broken.");
                    e.printStackTrace();
                }

                if (url != null) {
                    URLConnection connection = null;
                    try {
                        connection = url.openConnection();
                    } catch (IOException e) {
                        NoPlayerJoinLagDeath.pluginmessage("§4Can't Open Connection.");
                        e.printStackTrace();
                    }

                    if (connection != null) {
                        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = null;
                        try {
                            dBuilder = dbFactory.newDocumentBuilder();
                        } catch (ParserConfigurationException e) {
                            NoPlayerJoinLagDeath.pluginmessage("§4Parsing failure.");
                            e.printStackTrace();
                        }
                        if (dBuilder != null) {
                            Document doc = null;
                            try {
                                doc = dBuilder.parse((InputStream) connection.getContent());
                            } catch (SAXException | IOException e) {
                                NoPlayerJoinLagDeath.pluginmessage("§4Cant get Content from URL.");
                                e.printStackTrace();
                            }
                            if (doc != null) {
                                latestversion = doc.getElementsByTagName("version").item(0).getTextContent();
                            }
                        }
                    }
                }
            }
        }, 0L, 20 * 60 * 60 * 24); // run every day


        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                if (latestversion != null) {
                    if (!currentversion.equals(latestversion)) {
                        NoPlayerJoinLagDeath.pluginmessage("§7A new update for §b" + plugin.getDescription().getName() + "§7 is available! (§e" + currentversion + " §7-> §a" + latestversion + "§7)\nDownload: §b" + plugin.getDescription().getWebsite() + "§7.");
                    }
                } else {
                    NoPlayerJoinLagDeath.pluginmessage("§4Could not check for Updates.");
                }
            }
        }, 20L);

    }
}
