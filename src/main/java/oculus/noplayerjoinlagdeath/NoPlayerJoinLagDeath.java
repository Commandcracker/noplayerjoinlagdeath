package oculus.noplayerjoinlagdeath;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.*;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.ProjectileSource;

import java.util.ArrayList;

public class NoPlayerJoinLagDeath extends JavaPlugin implements Listener {
    private static NoPlayerJoinLagDeath intance = null;
    private static final ArrayList<Player> protectedplayers = new ArrayList();
    private static String message;

    public void onEnable() {
        new MetricsLite(this, 7803);
        NoPlayerJoinLagDeath.intance = this;

        saveDefaultConfig();
        reloadConfig();

        message = getConfig().getString("Message");

        Bukkit.getPluginManager().registerEvents(this, this);
        if (getConfig().getBoolean("Updater.Enabled")) {
            Bukkit.getPluginManager().registerEvents(new Updater(), this);
        }
    }

    public static NoPlayerJoinLagDeath getInstance() { return NoPlayerJoinLagDeath.intance; }

    @Override
    public void onDisable() { NoPlayerJoinLagDeath.intance = null; }

    public static void pluginmessage(String msg) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.hasPermission("sleep.notify")) {
                Bukkit.getServer().getConsoleSender().sendMessage("§b[" + getInstance().getDescription().getPrefix() + "] " + msg);
                p.sendMessage("§b[" + getInstance().getDescription().getPrefix() + "] " + msg);
            }
        }
    }

    public static boolean check(Player p) {
        if (protectedplayers.contains(p)) {
            if (p.isSneaking()) {
                unprotect(p);
                return false;
            }
            return true;
        }
        return false;
    }

    public static void unprotect(Player p) {
        protectedplayers.remove(p);
        if (p.getGameMode() == GameMode.SURVIVAL) {
            p.setAllowFlight(false);
        }
    }

    // on join Freeze and god
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        protectedplayers.add(p);
        p.setAllowFlight(true);
        p.sendMessage(message);
    }

    // GOD
    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (!e.isCancelled() && e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if (check(p)) {
                e.setCancelled(true);
            }
        }
    }

    // No Damaging
    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            if (e.getDamager() instanceof Player) {
                Player p = (Player) e.getDamager();
                if (check(p)) {
                    p.sendMessage(message);
                    e.setCancelled(true);
                }
            } else if (e.getDamager() instanceof Projectile) {
                Projectile projectile = (Projectile) e.getDamager();
                ProjectileSource projectileSource = projectile.getShooter();
                if (projectileSource != null) {
                    if (projectileSource instanceof Player) {
                        Player p = (Player) projectileSource;
                        if (check(p)) {
                            p.sendMessage(message);
                            e.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    // No Move
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (!e.isCancelled() && check(e.getPlayer())) {
            if (e.getPlayer().getLocation().getX() != e.getFrom().getX() || e.getPlayer().getLocation().getZ() != e.getFrom().getZ() || e.getPlayer().getLocation().getYaw() != e.getFrom().getYaw() || e.getPlayer().getLocation().getPitch() != e.getFrom().getPitch()) {
                e.getPlayer().sendMessage(message);
            }
            e.getPlayer().teleport(e.getFrom());
            e.setCancelled(true);
        }
    }

    // on chat disable
    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent e) {
        if (!e.isCancelled() && check(e.getPlayer())) {
            Player p = e.getPlayer();
            unprotect(p);
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
        if (!e.isCancelled() && check(e.getPlayer())) {
            Player p = e.getPlayer();
            unprotect(p);
            p.setAllowFlight(false);
            e.setCancelled(true);
        }
    }

    // No Mob Target
    @EventHandler
    public void onEntityTarget(EntityTargetEvent e) {
        if (!e.isCancelled() && e.getTarget() instanceof Player) {
            Player p = (Player) e.getTarget();
            if (check(p)) {
                e.setCancelled(true);
            }
        }
    }

    // NO Block Placing
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if  (!e.isCancelled() && check(e.getPlayer())) {
            e.getPlayer().sendMessage(message);
            e.setCancelled(true);
        }
    }

    // NO Block Breaking
    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if  (!e.isCancelled() && check(e.getPlayer())) {
            e.getPlayer().sendMessage(message);
            e.setCancelled(true);
        }
    }

    // NO Drop
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if  (!e.isCancelled() && check(e.getPlayer())) {
            e.getPlayer().sendMessage(message);
            e.setCancelled(true);
        }
    }

    // NO Inventory Drag
    @EventHandler
    public void onInventoryDrag(InventoryDragEvent e) {
        if  (!e.isCancelled()) {
            Player p = (Player) e.getWhoClicked();
            if (check(p)) {
                p.sendMessage(message);
                e.setCancelled(true);
            }
        }
    }

    // NO Inventory Click
    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if  (!e.isCancelled()) {
            Player p = (Player) e.getWhoClicked();
            if (check(p)) {
                p.sendMessage(message);
                e.setCancelled(true);
            }
        }
    }

    // NO Interaction
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (!e.isCancelled() && check(e.getPlayer())) {
            e.getPlayer().sendMessage(message);
            e.setCancelled(true);
        }
    }

    // No Bows
    @EventHandler
    public void onLaunchEvent(ProjectileLaunchEvent e) {
        if (!e.isCancelled() && e.getEntity().getShooter() instanceof Player) {
            Player p = (Player) e.getEntity().getShooter();
            if (check(p)) {
                p.sendMessage(message);
                e.setCancelled(true);
            }
        }
    }

    // No Fly
    @EventHandler
    public void onToggleFly(PlayerToggleFlightEvent e) {
        if (!e.isCancelled() && check(e.getPlayer())) {
            e.getPlayer().sendMessage(message);
            e.setCancelled(true);
        }
    }

}
